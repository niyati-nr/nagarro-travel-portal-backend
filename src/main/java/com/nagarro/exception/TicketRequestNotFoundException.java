package com.nagarro.exception;

public class TicketRequestNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public TicketRequestNotFoundException(Long id) {
	    super("Could not find ticket request " + id);
	  }
}
