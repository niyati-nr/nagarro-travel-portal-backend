package com.nagarro.exception;

public class AirportNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public AirportNotFoundException(Long id) {
	    super("Could not find airport " + id);
	  }
}
