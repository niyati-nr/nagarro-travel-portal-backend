package com.nagarro.exception;

public class CityNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public CityNotFoundException(Integer id) {
	    super("Could not find city " + id);
	  }
}
