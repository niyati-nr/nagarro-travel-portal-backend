package com.nagarro.exception;

public class DocumentNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public DocumentNotFoundException(Long id) {
	    super("Could not find document " + id);
	  }

}
