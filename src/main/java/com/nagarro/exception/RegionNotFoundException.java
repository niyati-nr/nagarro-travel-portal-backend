package com.nagarro.exception;

public class RegionNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public RegionNotFoundException(Integer id) {
	    super("Could not find region " + id);
	  }
}
