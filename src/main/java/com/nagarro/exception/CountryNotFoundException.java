package com.nagarro.exception;

public class CountryNotFoundException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public CountryNotFoundException(Short id) {
	    super("Could not find country " + id);
	  }
}
