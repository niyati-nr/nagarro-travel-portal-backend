package com.nagarro.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nagarro.models.Address;
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
