package com.nagarro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nagarro.models.Document;
@Repository
public interface DocumentRepository extends JpaRepository<Document, Long>  {
	@Query(value = "SELECT * FROM DOCUMENTS WHERE REQUEST_ID = ?1", nativeQuery = true)
	List<Document> findByRequestId(Long request_id);
}
