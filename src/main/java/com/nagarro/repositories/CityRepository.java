package com.nagarro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nagarro.models.City;
@Repository
public interface CityRepository extends JpaRepository<City, Integer> {
	@Query(value = "SELECT * FROM CITIES WHERE REGION_ID = ?1", nativeQuery = true)
	List<City> findByRegionId(Integer region_id);
}
