package com.nagarro.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nagarro.models.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	Employee findByUsernameAndPassword(String username, String password);
	@Query(value = "SELECT * FROM EMPLOYEES WHERE EMAIL = ?1", nativeQuery = true)
	Employee findByEmail(String email);
}