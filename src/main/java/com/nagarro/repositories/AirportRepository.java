package com.nagarro.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nagarro.models.Airport;
@Repository
public interface AirportRepository extends JpaRepository<Airport, Long>  {

}
