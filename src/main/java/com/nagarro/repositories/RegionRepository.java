package com.nagarro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nagarro.models.Region;
@Repository
public interface RegionRepository extends JpaRepository<Region, Integer>  {
	@Query(value = "SELECT * FROM REGIONS WHERE COUNTRY_ID = ?1", nativeQuery = true)
	List<Region> findByCountryId(Long country_id);
}
