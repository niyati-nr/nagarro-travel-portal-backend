package com.nagarro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nagarro.models.TicketRequest;
@Repository
public interface TicketRequestRepository extends JpaRepository<TicketRequest, Long> {
	@Query(value = "SELECT * FROM TICKETREQUESTS WHERE EMP_ID = ?1", nativeQuery = true)
	List<TicketRequest> findByEmployeeId(Long emp_id);
	@Query(value = "SELECT * FROM TICKETREQUESTS WHERE REQUEST_STATUS IN('SUBMITTED','RESUBMITTED','IN_PROCESS')", nativeQuery = true)
	List<TicketRequest> findActive();
}