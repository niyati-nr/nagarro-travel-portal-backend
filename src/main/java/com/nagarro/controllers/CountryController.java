package com.nagarro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.exception.CountryNotFoundException;
import com.nagarro.models.Country;
import com.nagarro.repositories.CountryRepository;

@RestController
public class CountryController {
	private final CountryRepository repository;

	CountryController(CountryRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/countries")
	List<Country> all() {
		return repository.findAll();
	}
	@GetMapping("/countries/{id}")
	Country one(@PathVariable Short id) {

		return repository.findById(id)
			      .orElseThrow(() -> new CountryNotFoundException(id));
	}

}
