package com.nagarro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.exception.RegionNotFoundException;
import com.nagarro.models.Region;
import com.nagarro.repositories.RegionRepository;

@RestController
public class RegionController {
	private final RegionRepository repository;

	RegionController(RegionRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/regions")
	List<Region> all() {
		return repository.findAll();
	}
	@GetMapping("/regions/{id}")
	Region one(@PathVariable Integer id) {

		return repository.findById(id)
			      .orElseThrow(() -> new RegionNotFoundException(id));
	}
	@GetMapping("/regions/country/{country_id}")
	List<Region> findByCountry(@PathVariable Long country_id) {
		return repository.findByCountryId(country_id);
	}
}
