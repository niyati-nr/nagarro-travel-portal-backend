package com.nagarro.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.exception.AirportNotFoundException;
import com.nagarro.models.Airport;
import com.nagarro.repositories.AirportRepository;

@RestController
class AirportController {

	private final AirportRepository repository;

	AirportController(AirportRepository repository) {
		this.repository = repository;
	}
	@GetMapping("/airports")
	CollectionModel<EntityModel<Airport>> all() {

		List<EntityModel<Airport>> airports = repository.findAll().stream()
				.map(airport -> EntityModel.of(airport,
						linkTo(methodOn(AirportController.class).one(airport.getAirport_id())).withSelfRel(),
						linkTo(methodOn(AirportController.class).all()).withRel("airports")))
				.collect(Collectors.toList());

		return CollectionModel.of(airports, linkTo(methodOn(AirportController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/airports")
	Airport newAirport(@RequestBody Airport newAirport) {
		return repository.save(newAirport);
	}

	// Single item

	// tag::get-single-item[]
	@GetMapping("/airports/{id}")
	EntityModel<Airport> one(@PathVariable Long id) {

		Airport airport = repository.findById(id) //
				.orElseThrow(() -> new AirportNotFoundException(id));

		return EntityModel.of(airport, //
				linkTo(methodOn(AirportController.class).one(id)).withSelfRel(),
				linkTo(methodOn(AirportController.class).all()).withRel("airports"));
	}
	// end::get-single-item[]

	@PutMapping("/airports/{id}")
	Airport replaceAirport(@RequestBody Airport newAirport, @PathVariable Long id) {

		return repository.findById(id) //
				.map(airport -> {
					airport.setAirport_name(newAirport.getAirport_name());
					airport.setCountry_code(newAirport.getCountry_code());
					airport.setIata_code(newAirport.getIata_code());
					airport.setRegion_name(newAirport.getRegion_name());
					return repository.save(airport);
				}) //
				.orElseGet(() -> {
					newAirport.setAirport_id(id);
					return repository.save(newAirport);
				});
	}
	
	@PatchMapping("/airports/{id}")
	Airport updateAirport(@RequestBody Airport newAirport, @PathVariable Long id) {
	    
	    return repository.findById(id)
	      .map(airport -> {
	    	  airport.setAirport_name(newAirport.getAirport_name());
				airport.setCountry_code(newAirport.getCountry_code());
				airport.setIata_code(newAirport.getIata_code());
				airport.setRegion_name(newAirport.getRegion_name());
	        return repository.save(airport);
	      })
	      .orElseGet(() -> {
	        newAirport.setAirport_id(id);
	        return repository.save(newAirport);
	      });
	  }

	@DeleteMapping("/airports/{id}")
	void deleteAirport(@PathVariable Long id) {
		repository.deleteById(id);
	}
}
