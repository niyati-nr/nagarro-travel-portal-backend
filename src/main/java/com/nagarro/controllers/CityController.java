package com.nagarro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.exception.CityNotFoundException;
import com.nagarro.models.City;
import com.nagarro.repositories.CityRepository;

@RestController
public class CityController {
	private final CityRepository repository;

	CityController(CityRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/cities")
	List<City> all() {
		return repository.findAll();
	}
	@GetMapping("/cities/{id}")
	City one(@PathVariable Integer id) {
		return repository.findById(id)
			      .orElseThrow(() -> new CityNotFoundException(id));
	}
	@GetMapping("/cities/region/{region_id}")
	List<City> findByRegion(@PathVariable Integer region_id) {
		return repository.findByRegionId(region_id);
	}
}
