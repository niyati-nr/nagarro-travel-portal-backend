package com.nagarro.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.nagarro.models.TicketRequest;

@Component
public class EmailHandler {
	@Autowired
    private JavaMailSender javaMailSender;
	public void sendEmail(String email, String username, String password) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);

        msg.setSubject("Welcome to Explorers Hub!");
        msg.setText("Hello, little explorer!\n\nThank you for joining Explorers Hub!\nWe are extremely happy to have you as a part of our family and we can't wait to enjoy tons of exciting adventures with you.\n\nTo get you started, here are your login credentials:\n\nUsername:"+username+"\nPassword:"+password+"\n\nHave fun exploring with Explorers Hub!\n\nRegards,\nNiyati Rana\nExplorers Hub Admin");

        javaMailSender.send(msg);

    }
	public void forgotCredentialsEmail(String email, String username, String password) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);

        msg.setSubject("Nagarro Travel Portal Information");
        msg.setText("Hello!\n\nYou have requested your user name and password for the your access to the Nagarro Travel Portal:\n\nUsername: "+username+"\nPassword: "+password+"\n\nPlease contact the Travel team if you have any questions.\n\nThank you,\nNagarro Travel Team");

        javaMailSender.send(msg);
	}
	public void ticketRaisedEmail(String email,TicketRequest ticketRequest) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);

        msg.setSubject("Success - Request Ticket Raised");
        msg.setText("Hello!\n\nYou have successfully submitted your Request!\n\nHere are your ticket details:\n\nTicket ID: "+ticketRequest.getRequest_id()+"\nTicket Status: "+ticketRequest.getRequestStatus()+
        		"\n\nPlease contact the Travel team if you have any questions.\n\nThank you,\nNagarro Travel Team");

        javaMailSender.send(msg);
	}
	public void changePasswordEmail(String email, String username, String password) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);

        msg.setSubject("Nagarro Travel Portal Information");
        msg.setText("Hello!\n\nYou have successfully changed your password.\n\nYour new credentials are as follows:\n\nUsername: "+username+"\nPassword: "+password+"\n\nPlease contact the Travel team if you have any questions.\n\nThank you,\nNagarro Travel Team");

        javaMailSender.send(msg);
	}
}
