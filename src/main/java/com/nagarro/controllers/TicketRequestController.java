package com.nagarro.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.common.RequestExpenseHandler;
import com.nagarro.common.RequestPriority;
import com.nagarro.common.RequestStatus;
import com.nagarro.common.RequestType;
import com.nagarro.exception.TicketRequestNotFoundException;
import com.nagarro.models.Airport;
import com.nagarro.models.City;
import com.nagarro.models.Employee;
import com.nagarro.models.TicketRequest;
import com.nagarro.models.TicketRequestFormModel;
import com.nagarro.repositories.TicketRequestRepository;
import com.nagarro.utils.DateParser;
@RestController
public class TicketRequestController {
	@Autowired
	AirportController airportController;
	@Autowired
	EmployeeController employeeController;
	@Autowired
	EmailHandler emailHandler;
	@Autowired
	CityController cityController;
	private final TicketRequestRepository repository;

	TicketRequestController(TicketRequestRepository repository) {
		this.repository = repository;
	}
	
	/**
	 * This method returns all the ticket requests
	 * @return Collection of All Ticket Requests in the database
	 */
	@GetMapping("/tickets")
	List<TicketRequest> all() {

		return repository.findAll();
	}
	/**
	 * This method is used to get the Ticket Request with having a given id
	 * @param id The ticket request id
	 * @return The corresponding Ticket Request
	 */
	@GetMapping("/tickets/{id}")
	EntityModel<TicketRequest> one(@PathVariable Long id) {

		TicketRequest ticket = repository.findById(id) //
				.orElseThrow(() -> new TicketRequestNotFoundException(id));

		return EntityModel.of(ticket, //
				linkTo(methodOn(TicketRequestController.class).one(id)).withSelfRel(),
				linkTo(methodOn(TicketRequestController.class).all()).withRel("tickets"));
	}
	/**
	 * Delete a Ticket Request from database
	 * @param id The id of the ticket request to be deleted
	 */
	
	@DeleteMapping("/tickets/{id}")
	void deleteTicketRequest(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
	/**
	 * Add a new Ticket Request with reference to the employee
	 * @param emp_id The employee id
	 * @param ticketRequestFormModel The form model
	 * @return The raise Ticket Request
	 * @throws Exception If there is a problem while raising the ticket
	 */
	@PostMapping("/tickets/raiseticket/{emp_id}")
	TicketRequest raiseTicket(@PathVariable Long emp_id, @RequestBody TicketRequestFormModel ticketRequestFormModel) throws Exception {
		Employee employee = employeeController.one(emp_id).getContent();
	    TicketRequest ticketRequest = new TicketRequest();
	    ticketRequest.setAdditional_details(ticketRequestFormModel.getAdditional_details());
	    ticketRequest.setAmount_limit(ticketRequestFormModel.getAmount_limit());
	    City originCity = cityController.one(ticketRequestFormModel.getOrigin_city());
	    City destinationCity = cityController.one(ticketRequestFormModel.getDestination_city());
//	    Airport originAirport = (airportController.one(ticketRequestFormModel.getOrigin_id())).getContent();
//	    Airport destinationAirport = (airportController.one(ticketRequestFormModel.getDestination_id())).getContent();
//	    ticketRequest.setDestination(destinationAirport);
//	    ticketRequest.setOrigin(originAirport);
	    ticketRequest.setOrigin_city(originCity);
	    ticketRequest.setDestination_city(destinationCity);
	    ticketRequest.setDocuments(null);
	    ticketRequest.setAdmin_remarks(null);
	    ticketRequest.setEnd_date(DateParser.parseStringToUtilDate(ticketRequestFormModel.getEnd_date()));
	    ticketRequest.setStart_date(DateParser.parseStringToUtilDate(ticketRequestFormModel.getStart_date()));
	    ticketRequest.setExpense_handler(RequestExpenseHandler.valueOf(ticketRequestFormModel.getExpense_handler().toUpperCase()));
	    ticketRequest.setPassport_number(ticketRequestFormModel.getPassport_number());
	    ticketRequest.setProject_name(ticketRequestFormModel.getProject_name());
	    ticketRequest.setRequestPriority(RequestPriority.valueOf(ticketRequestFormModel.getRequestPriority().toUpperCase()));
	    ticketRequest.setRequestType(RequestType.valueOf(ticketRequestFormModel.getRequestType().toUpperCase()));
	    ticketRequest.setTravel_approver(ticketRequestFormModel.getTravel_approver());
	    ticketRequest.setTravel_duration(ticketRequestFormModel.getTravel_duration());
	    ticketRequest.setRequestStatus(RequestStatus.SUBMITTED);
	    ticketRequest.setRef_employee(employee);
	    //Formatting date for accurate util date to mysql timestamp conversion
	    java.util.Date lastupdateddate = new java.util.Date();
	    java.text.SimpleDateFormat sdf = 
	    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String currentTime = sdf.format(lastupdateddate);
	    ticketRequest.setLastUpdated(currentTime);
	    //-----------------------------------------------------------//
	    TicketRequest ticketRequestSaved = null;
	    try {
	    	ticketRequestSaved = repository.save(ticketRequest);
	    	emailHandler.ticketRaisedEmail(employee.getEmail(),ticketRequestSaved);
	    	return ticketRequestSaved;
	    }catch(Exception e) {
	    	throw new Exception("Error while raising ticket. Please try again.");
	    }
	  }
	/**
	 * This method is used to edit a particular ticket request
	 * @param id The ticket request id
	 * @param ticketRequestFormModel  The form model object
	 * @return The edited/updated Ticket Request
	 */
	@PatchMapping("/tickets/editticket/{id}")
	TicketRequest editTicket(@PathVariable Long id, @RequestBody TicketRequestFormModel ticketRequestFormModel) {
	    return repository.findById(id) //
				.map(ticketRequest -> {
					if(ticketRequestFormModel.getAdditional_details()!=null&&!ticketRequestFormModel.getAdditional_details().isEmpty()) {
	    ticketRequest.setAdditional_details(ticketRequestFormModel.getAdditional_details());
					}
					if(ticketRequestFormModel.getAmount_limit()!=null&&!ticketRequestFormModel.getAmount_limit().isEmpty()) {
	    ticketRequest.setAmount_limit(ticketRequestFormModel.getAmount_limit());
					}
	    if(ticketRequestFormModel.getOrigin_city()!=0) {
	    	City originCity = cityController.one(ticketRequestFormModel.getOrigin_city());
	    	ticketRequest.setOrigin_city(originCity);
	    }
	    if(ticketRequestFormModel.getDestination_city()!=0) {
	    	City destinationCity = cityController.one(ticketRequestFormModel.getDestination_city());
		    ticketRequest.setDestination_city(destinationCity);
	    }
	    if(ticketRequestFormModel.getEnd_date()!=null&&!ticketRequestFormModel.getEnd_date().isEmpty()) {
	    	ticketRequest.setEnd_date(DateParser.parseStringToUtilDate(ticketRequestFormModel.getEnd_date()));
	    }
	    if(ticketRequestFormModel.getStart_date()!=null&&!ticketRequestFormModel.getStart_date().isEmpty()) {
	    	ticketRequest.setStart_date(DateParser.parseStringToUtilDate(ticketRequestFormModel.getStart_date()));
	    }
	    if(ticketRequestFormModel.getExpense_handler()!=null&&!ticketRequestFormModel.getExpense_handler().isEmpty()) {
	    	ticketRequest.setExpense_handler(RequestExpenseHandler.valueOf(ticketRequestFormModel.getExpense_handler().toUpperCase()));
	    }
	    if(ticketRequestFormModel.getPassport_number()!=null&&!ticketRequestFormModel.getPassport_number().isEmpty()) {
	    	ticketRequest.setPassport_number(ticketRequestFormModel.getPassport_number());
	    }
	    if(ticketRequestFormModel.getProject_name()!=null&&!ticketRequestFormModel.getProject_name().isEmpty()) {
	    	ticketRequest.setProject_name(ticketRequestFormModel.getProject_name());
	    }
	    if(ticketRequestFormModel.getRequestPriority()!=null&&!ticketRequestFormModel.getRequestPriority().isEmpty()) {
	    	ticketRequest.setRequestPriority(RequestPriority.valueOf(ticketRequestFormModel.getRequestPriority().toUpperCase()));
	    }
	    if(ticketRequestFormModel.getRequestType()!=null&&!ticketRequestFormModel.getRequestType().isEmpty()) {
	    	ticketRequest.setRequestType(RequestType.valueOf(ticketRequestFormModel.getRequestType().toUpperCase()));
	    }
	    if(ticketRequestFormModel.getTravel_approver()!=null&&!ticketRequestFormModel.getTravel_approver().isEmpty()) {
	    	ticketRequest.setTravel_approver(ticketRequestFormModel.getTravel_approver());
	    }
	    if(ticketRequestFormModel.getTravel_duration()!=null&&!ticketRequestFormModel.getTravel_duration().isEmpty()) {
	    	ticketRequest.setTravel_duration(ticketRequestFormModel.getTravel_duration());
	    }
	    if(ticketRequestFormModel.getAdmin_remarks()!=null&&!ticketRequestFormModel.getAdmin_remarks().isEmpty()) {
	    	ticketRequest.setAdmin_remarks(ticketRequestFormModel.getAdmin_remarks());
	    }
	    if(ticketRequest.getRequestStatus().equals(RequestStatus.APPROVED)||ticketRequest.getRequestStatus().equals(RequestStatus.IN_PROCESS)||ticketRequest.getRequestStatus().equals(RequestStatus.REJECTED)||ticketRequest.getRequestStatus().equals(RequestStatus.DONE)) {
	    	ticketRequest.setRequestStatus(RequestStatus.RESUBMITTED);
	    }
	    java.util.Date lastupdateddate = new java.util.Date();
	    java.text.SimpleDateFormat sdf = 
	    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String currentTime = sdf.format(lastupdateddate);
	    ticketRequest.setLastUpdated(currentTime);
	    return repository.save(ticketRequest);
				}).orElseThrow(() -> new TicketRequestNotFoundException(id));
	  }
	
	/**
	 * Get all tickets of an employee
	 * @param emp_id The id of the employee
	 * @return The collection of all ticket requests
	 */
//	@GetMapping("/tickets/employee/{emp_id}")
//	CollectionModel<EntityModel<TicketRequest>> getEmployeeTickets(@PathVariable Long emp_id) {
//		List<EntityModel<TicketRequest>> tickets = repository.findByEmployeeId(emp_id).stream()
//				.map(ticket -> EntityModel.of(ticket,
//						linkTo(methodOn(TicketRequestController.class).one(ticket.getRequest_id())).withSelfRel(),
//						linkTo(methodOn(TicketRequestController.class).all()).withRel("tickets")))
//				.collect(Collectors.toList());
//
//		return CollectionModel.of(tickets, linkTo(methodOn(TicketRequestController.class).all()).withSelfRel());
//	}
	@GetMapping("/tickets/employee/{emp_id}")
	List<TicketRequest> getEmployeeTickets(@PathVariable Long emp_id) {
		return repository.findByEmployeeId(emp_id);
	}
	/**
	 * This method changes the status of a given Ticket Request
	 * @param id Ticket Request ID
	 * @param status The new status
	 * @return The updated Ticket Request
	 */
	@PatchMapping("/tickets/changestatus/{id}")
	TicketRequest changeTicketStatus(@PathVariable Long id, @RequestParam String status) {
		return repository.findById(id) //
				.map(ticketRequest -> {
					ticketRequest.setRequestStatus(RequestStatus.valueOf(status.toUpperCase()));
					return repository.save(ticketRequest);
				}).orElseThrow(() -> new TicketRequestNotFoundException(id));
	}
	@PatchMapping("/tickets/adminremarks/{id}")
	TicketRequest addAdminRemarks(@PathVariable Long id, @RequestParam String remarks) {
		return repository.findById(id) //
				.map(ticketRequest -> {
					ticketRequest.setAdmin_remarks(remarks);
					return repository.save(ticketRequest);
				}).orElseThrow(() -> new TicketRequestNotFoundException(id));
	}
	/**
	 * Get all active tickets
	 */
	@GetMapping("/tickets/active")
	List<TicketRequest> getActiveTickets() {
		return repository.findActive();
	}
}
