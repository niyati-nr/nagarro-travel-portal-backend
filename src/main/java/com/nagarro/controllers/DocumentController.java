package com.nagarro.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nagarro.exception.DocumentNotFoundException;
import com.nagarro.models.Document;
import com.nagarro.models.TicketRequest;
import com.nagarro.repositories.DocumentRepository;

@RestController
class DocumentController {
	@Autowired
	TicketRequestController ticketRequestController;
	private final DocumentRepository repository;
	DocumentController(DocumentRepository repository) {
		this.repository = repository;
	}
	@GetMapping("/documents")
	CollectionModel<EntityModel<Document>> all() {

		List<EntityModel<Document>> documents = repository.findAll().stream()
				.map(document -> EntityModel.of(document,
						linkTo(methodOn(DocumentController.class).one(document.getDocument_id())).withSelfRel(),
						linkTo(methodOn(DocumentController.class).all()).withRel("documents")))
				.collect(Collectors.toList());

		return CollectionModel.of(documents, linkTo(methodOn(DocumentController.class).all()).withSelfRel());
	}

	@PostMapping("/documents/{ticket_id}")
	Document newDocument(@RequestParam("file") MultipartFile file,@PathVariable Long ticket_id,@RequestParam("filename") String filename,@RequestParam("filedesc") String filedesc) throws IOException {
		TicketRequest ticketRequest = ticketRequestController.one(ticket_id).getContent();
		Document document = new Document();
		document.setRef_request(ticketRequest);
		byte[] fileContent=file.getBytes();
		document.setFile(fileContent);		
		document.setSize(file.getSize());
		document.setDocumentName(filename);
		document.setDescription(filedesc);
		return repository.save(document);
	}

	@GetMapping("/documents/{id}")
	EntityModel<Document> one(@PathVariable Long id) {
		Document document = repository.findById(id) //
				.orElseThrow(() -> new DocumentNotFoundException(id));

		return EntityModel.of(document, //
				linkTo(methodOn(DocumentController.class).one(id)).withSelfRel(),
				linkTo(methodOn(DocumentController.class).all()).withRel("documents"));
	}
	
	@GetMapping("/documents/ticket/{id}")
	CollectionModel<EntityModel<Document>> findByTicket(@PathVariable Long id) {
		List<EntityModel<Document>> documents = repository.findByRequestId(id).stream()
				.map(document -> EntityModel.of(document,
						linkTo(methodOn(DocumentController.class).one(document.getDocument_id())).withSelfRel(),
						linkTo(methodOn(DocumentController.class).all()).withRel("documents")))
				.collect(Collectors.toList());

		return CollectionModel.of(documents, linkTo(methodOn(DocumentController.class).all()).withSelfRel());
	}
	
/*	@PatchMapping("/documents/{id}")
	Document updateDocument(@RequestBody Document newDocument, @PathVariable Long id) {
	    
	    return repository.findById(id)
	      .map(document -> {
	    	  document.setDocumentName(newDocument.getDocumentName());
				document.setFile(newDocument.getFile());
				document.setDescription(newDocument.getDescription());
				document.setRef_request(newDocument.getRef_request());
				document.setSize(newDocument.getSize());
				return repository.save(document);
	      })
	      .orElseGet(() -> {
	    	  newDocument.setDocument_id(id);
				return repository.save(newDocument);
	      });
	  }*/

	@DeleteMapping("/documents/{id}")
	void deleteDocument(@PathVariable Long id) {
		repository.deleteById(id);
	}
}
