package com.nagarro.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.common.AddressType;
import com.nagarro.exception.EmployeeNotFoundException;
import com.nagarro.models.Address;
import com.nagarro.models.Employee;
import com.nagarro.models.LoginModel;
import com.nagarro.models.RegisterModel;
import com.nagarro.repositories.EmployeeRepository;
import com.nagarro.utils.PasswordGenerator;

@RestController
class EmployeeController {
	private final EmployeeRepository repository;
	@Autowired
	EmailHandler emailHandler;
	EmployeeController(EmployeeRepository repository) {
		this.repository = repository;
	}
	@GetMapping("/employees")
	CollectionModel<EntityModel<Employee>> all() {

		List<EntityModel<Employee>> employees = repository.findAll().stream()
				.map(employee -> EntityModel.of(employee,
						linkTo(methodOn(EmployeeController.class).one(employee.getEmp_id())).withSelfRel(),
						linkTo(methodOn(EmployeeController.class).all()).withRel("employees")))
				.collect(Collectors.toList());

		return CollectionModel.of(employees, linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
	}

	@GetMapping("/employees/{id}")
	EntityModel<Employee> one(@PathVariable Long id) {

		Employee employee = repository.findById(id) //
				.orElseThrow(() -> new EmployeeNotFoundException(id));

		return EntityModel.of(employee, //
				linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel(),
				linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
	}
	
	@PatchMapping("/employees/{id}")
	  Employee updateEmployee(@RequestBody RegisterModel employeeEditModel, @PathVariable Long id) {
	    return repository.findById(id)
	      .map(employee -> {
	    	  Address address1=new Address();
		  		Address address2=new Address();
		  		//Filling Address 1
		  		address1.setAddressLine(employeeEditModel.getAddressline1());
		  		address1.setAddressType(AddressType.PRIMARY);
		  		address1.setCity(employeeEditModel.getCity1());
		  		address1.setState(employeeEditModel.getState1());
		  		address1.setCountry(employeeEditModel.getCountry1());
		  		address1.setZipcode(employeeEditModel.getZipcode1());
		  		employee.setPrimaryaddress(address1);
		  		//Filling Address 2
		  		if(employeeEditModel.getAddressline2()!=null&&!employeeEditModel.getAddressline2().isEmpty()) {
		  			address2.setAddressLine(employeeEditModel.getAddressline2());
		  			address2.setAddressType(AddressType.SECONDARY);
		  			address2.setCity(employeeEditModel.getCity2());
		  			address2.setState(employeeEditModel.getState2());
		  			address2.setCountry(employeeEditModel.getCountry2());
		  			address2.setZipcode(employeeEditModel.getZipcode2());
		  			employee.setSecondaryaddress(address2);
		  		}
		  		else {
		  			employee.setSecondaryaddress(null);
		  		}
		  		//Filling Employee Details
		  		employee.setFirstname(employeeEditModel.getFirstname());
		  		employee.setLastname(employeeEditModel.getLastname());
		  		employee.setBusinessunit(employeeEditModel.getBusinessunit());
		  		employee.setEmail(employeeEditModel.getEmail());
		  		employee.setJobtitle(employeeEditModel.getJobtitle());
		  		employee.setPrimaryaddress(address1);
		  		employee.setTelephone(employeeEditModel.getTelephone());
	        return repository.save(employee);
	      })
	      .orElseGet(() -> {
	        return null;
	      });
	  }

	@DeleteMapping("/employees/{id}")
	void deleteEmployee(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
	@PostMapping("/employees/login")
	EntityModel<Employee> login(@RequestBody LoginModel loginModel) throws Exception {
		Employee employee = repository.findByUsernameAndPassword(loginModel.getUsername(), PasswordGenerator.encodePassword(loginModel.getPassword()));
		if(employee==null) {
			return null;
		}
		return EntityModel.of(employee, //
				linkTo(methodOn(EmployeeController.class).one(employee.getEmp_id())).withSelfRel(),
				linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
	}
	@PostMapping("/employees/register")
	Employee newEmployee(@RequestBody RegisterModel registerModel) throws Exception {
		Employee newEmployee = new Employee();
		Address address1=new Address();
		Address address2=new Address();
		//Filling Address 1
		address1.setAddressLine(registerModel.getAddressline1());
		address1.setAddressType(AddressType.valueOf(registerModel.getAddresstype1().toUpperCase().toUpperCase()));
		address1.setCity(registerModel.getCity1());
		address1.setState(registerModel.getState1());
		address1.setCountry(registerModel.getCountry1());
		address1.setZipcode(registerModel.getZipcode1());
		//Filling Address 2
		if(registerModel.getAddressline2()!=null&&!registerModel.getAddressline2().isEmpty()) {
			address2.setAddressLine(registerModel.getAddressline2());
			if(registerModel.getAddresstype2()!=null&&!registerModel.getAddresstype2().isEmpty()) {
			address2.setAddressType(AddressType.valueOf(registerModel.getAddresstype2().toUpperCase()));
			}
			address2.setCity(registerModel.getCity2());
			address2.setState(registerModel.getState2());
			address2.setCountry(registerModel.getCountry2());
			address2.setZipcode(registerModel.getZipcode2());
			newEmployee.setSecondaryaddress(address2);
		}
		else {
			newEmployee.setSecondaryaddress(null);
		}
		//Filling Employee Details
		newEmployee.setFirstname(registerModel.getFirstname());
		newEmployee.setLastname(registerModel.getLastname());
		newEmployee.setBusinessunit(registerModel.getBusinessunit());
		newEmployee.setEmail(registerModel.getEmail());
		newEmployee.setJobtitle(registerModel.getJobtitle());
		newEmployee.setPrimaryaddress(address1);
		newEmployee.setTelephone(registerModel.getTelephone());
		newEmployee.setUsername(registerModel.getEmail());
		newEmployee.setPassword(PasswordGenerator.encodePassword(PasswordGenerator.getAlphaNumericString()));
		try {
			Employee employee2 = repository.save(newEmployee);
			emailHandler.sendEmail(newEmployee.getEmail(), newEmployee.getUsername(), PasswordGenerator.decodePassword(newEmployee.getPassword()));
			return employee2;
		}catch(Exception e) {
			return null;
		}
	}
	@GetMapping("/employees/forgotcredentials/{id}")
	ResponseEntity<String> forgotCredentials(@PathVariable Long id) {
		Employee employee = repository.findById(id) //
				.orElseThrow(() -> new EmployeeNotFoundException(id));
		String password = PasswordGenerator.decodePassword(employee.getPassword());
		emailHandler.forgotCredentialsEmail(employee.getEmail(), employee.getUsername(), password);
		return new ResponseEntity<>(
			      HttpStatus.OK);
	}
	@GetMapping("/employees/checkemail/{email}")
	Long checkEmail(@PathVariable String email) {
		Employee employee = repository.findByEmail(email);
		if(employee==null) {
			return (long) -1;
		}
		else {
			return employee.getEmp_id();
		}
	}
	
	@PatchMapping("employees/resetpassword/{id}")
	ResponseEntity<String> resetPassword(@PathVariable Long id,@RequestBody String request) {
		String password = request.replaceAll("([{}])","");
		password=password.split(":")[1];
		password = password.trim();
		password=password.replaceAll("\"", "");
		Employee employee = repository.findById(id) //
				.orElseThrow(() -> new EmployeeNotFoundException(id));
		employee.setPassword(PasswordGenerator.encodePassword(password));
		try {
		repository.save(employee);
		emailHandler.changePasswordEmail(employee.getEmail(), employee.getUsername(), password);
		return new ResponseEntity<>(
			      HttpStatus.OK);
		}catch(Exception e) {
			return null;
		}
	}
}
