package com.nagarro.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.exception.AddressNotFoundException;
import com.nagarro.models.Address;
import com.nagarro.repositories.AddressRepository;

@RestController
class AddressController {

	private final AddressRepository repository;

	AddressController(AddressRepository repository) {
		this.repository = repository;
	}
	@GetMapping("/addresses")
	CollectionModel<EntityModel<Address>> all() {

		List<EntityModel<Address>> addresses = repository.findAll().stream()
				.map(address -> EntityModel.of(address,
						linkTo(methodOn(AddressController.class).one(address.getAddress_id())).withSelfRel(),
						linkTo(methodOn(AddressController.class).all()).withRel("addresses")))
				.collect(Collectors.toList());

		return CollectionModel.of(addresses, linkTo(methodOn(AddressController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/addresses")
	Address newAddress(@RequestBody Address newAddress) {
		return repository.save(newAddress);
	}

	// Single item

	// tag::get-single-item[]
	@GetMapping("/addresses/{id}")
	EntityModel<Address> one(@PathVariable Long id) {

		Address address = repository.findById(id) //
				.orElseThrow(() -> new AddressNotFoundException(id));

		return EntityModel.of(address, //
				linkTo(methodOn(AddressController.class).one(id)).withSelfRel(),
				linkTo(methodOn(AddressController.class).all()).withRel("addresses"));
	}
	// end::get-single-item[]

	@PutMapping("/addresses/{id}")
	Address replaceAddress(@RequestBody Address newAddress, @PathVariable Long id) {

		return repository.findById(id) //
				.map(address -> {
					address.setAddressLine(newAddress.getAddressLine());
					address.setAddressType(newAddress.getAddressType());
					address.setCity(newAddress.getCity());
					address.setState(newAddress.getState());
					address.setCountry(newAddress.getCountry());
					address.setZipcode(newAddress.getZipcode());
					return repository.save(address);
				}) //
				.orElseGet(() -> {
					newAddress.setAddress_id(id);
					return repository.save(newAddress);
				});
	}
	
	@PatchMapping("/addresses/{id}")
	Address updateAddress(@RequestBody Address newAddress, @PathVariable Long id) {
	    
	    return repository.findById(id)
	      .map(address -> {
	    	  address.setAddressLine(newAddress.getAddressLine());
				address.setAddressType(newAddress.getAddressType());
				address.setCity(newAddress.getCity());
				address.setState(newAddress.getState());
				address.setCountry(newAddress.getCountry());
				address.setZipcode(newAddress.getZipcode());
	        return repository.save(address);
	      })
	      .orElseGet(() -> {
	    	  newAddress.setAddress_id(id);
	        return repository.save(newAddress);
	      });
	  }

	@DeleteMapping("/addresses/{id}")
	void deleteAddress(@PathVariable Long id) {
		repository.deleteById(id);
	}
}

