package com.nagarro.common;

public enum RequestType {
	TRAVEL_TICKETS, HOTEL_STAY, VISA, WORK_PERMIT;
	public String getRequestType() {
		switch(this) {
			case TRAVEL_TICKETS:
				return "Travel Tickets";
			case HOTEL_STAY:
				return "Hotel Stay";
			case VISA:
				return "Visa";
			case WORK_PERMIT:
				return "Work Permit";
			default:
				return null;
		}
	}
	public static RequestType setRequestType(String str) {
		switch(str) {
			case "TRAVEL TICKETS":
				return TRAVEL_TICKETS;
			case "HOTEL STAY":
				return HOTEL_STAY;
			case "VISA":
				return VISA;
			case "WORK PERMIT":
				return WORK_PERMIT;
			default:
				return null;
		}
	}
}
