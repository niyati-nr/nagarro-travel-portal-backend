package com.nagarro.common;

public enum RequestExpenseHandler {
		CLIENT,NAGARRO;
		public String getRequestPriority() {
			switch(this) {
				case CLIENT:
					return "Client";
				case NAGARRO:
					return "Nagarro";
				default:
					return null;
			}
		}
}
