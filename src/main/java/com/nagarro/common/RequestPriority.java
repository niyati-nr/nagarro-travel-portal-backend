package com.nagarro.common;

public enum RequestPriority {
	NORMAL, URGENT, IMMEDIATE;
	public String getRequestPriority() {
		switch(this) {
			case NORMAL:
				return "Normal";
			case URGENT:
				return "Urgent";
			case IMMEDIATE:
				return "Immediate";
			default:
				return null;
		}
	}
}
