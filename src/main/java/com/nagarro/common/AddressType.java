package com.nagarro.common;

public enum AddressType {
	PRIMARY,SECONDARY;
	public String getAddressType() {
		switch(this) {
		case PRIMARY:
			return "Primary";
		case SECONDARY:
			return "Secondary";
		default:
			return null;
		}
	}
}
