package com.nagarro.common;

public enum RequestStatus {
	SUBMITTED,RESUBMITTED,IN_PROCESS,APPROVED,REJECTED,CANCELLED,DONE;
	public String getRequestStatus() {
		switch(this) {
			case SUBMITTED:
				return "Submitted";
			case RESUBMITTED:
				return "Resubmitted";
			case IN_PROCESS:
				return "In Process";
			case APPROVED:
				return "Approved";
			case REJECTED:
				return "Rejected";
			case CANCELLED:
				return "Cancelled";
			case DONE:
				return "Done";
			default:
				return null;
		}
	}
}
