package com.nagarro.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="employees")
public class Employee {
  private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) Long emp_id;
  @NotNull(message = "First Name is required")
  @Column(length=100, name="firstname")
  private String firstname;
  @NotNull(message = "Last Name is required")
  @Column(length=100, name="lastname")
  private String lastname;
  @NotNull(message = "Business Unit is required")
  @Column(length=50, name="businessunit")
  private String businessunit;
  @NotNull(message = "Employee Job Title is required")
  @Column(length=50,name="jobtitle")
  private String jobtitle;
  @NotNull(message = "Email Address is required")
  @Column(length=100, name="email", unique=true)
  private String email;
  @NotNull(message = "Telephone Number is required")
  @Column(length=15, name="telephone")
  private String telephone;
  @NotNull(message = "Primary Address is required")
  @OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
  @JoinColumn(name="fk_primaryaddress")
  private Address primaryaddress;
  @OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
  @JoinColumn(name="fk_secondaryaddress")
  private Address secondaryaddress;
  @Column(length=100, name="username", unique=true)
  private String username;
  @Column(length=100, name="password")
  private String password;
  @OneToMany(mappedBy="ref_employee",cascade=CascadeType.REMOVE)
  @JsonIgnore
  private Set<TicketRequest> ticketRequests;
  public Employee() {
	  
  }
	public Employee(Long id, String firstname,String lastname,String businessunit, String title,String email,String telephone,Address primaryaddress, Address secondaryaddress, String username, String password) {
	this.emp_id = id;
	this.firstname = firstname;
	this.lastname = lastname;
	this.businessunit = businessunit;
	this.jobtitle = title;
	this.email = email;
	this.telephone = telephone;
	this.primaryaddress = primaryaddress;
	this.secondaryaddress = secondaryaddress;
	this.username = username;
	this.password = password;
}
	public Long getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Long id) {
		this.emp_id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getBusinessunit() {
		return businessunit;
	}
	public void setBusinessunit(String businessunit) {
		this.businessunit = businessunit;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String title) {
		this.jobtitle = title;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Address getPrimaryaddress() {
		return primaryaddress;
	}
	public void setPrimaryaddress(Address primaryaddress) {
		this.primaryaddress = primaryaddress;
	}
	public Address getSecondaryaddress() {
		return secondaryaddress;
	}
	public void setSecondaryaddress(Address secondaryaddress) {
		this.secondaryaddress = secondaryaddress;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Set<TicketRequest> getTicketRequests() {
		return ticketRequests;
	}
	public void setTicketRequests(Set<TicketRequest> ticketRequests) {
		this.ticketRequests = ticketRequests;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessunit == null) ? 0 : businessunit.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((emp_id == null) ? 0 : emp_id.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((jobtitle == null) ? 0 : jobtitle.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((primaryaddress == null) ? 0 : primaryaddress.hashCode());
		result = prime * result + ((secondaryaddress == null) ? 0 : secondaryaddress.hashCode());
		result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
		result = prime * result + ((ticketRequests == null) ? 0 : ticketRequests.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (businessunit == null) {
			if (other.businessunit != null)
				return false;
		} else if (!businessunit.equals(other.businessunit))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (emp_id == null) {
			if (other.emp_id != null)
				return false;
		} else if (!emp_id.equals(other.emp_id))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (jobtitle == null) {
			if (other.jobtitle != null)
				return false;
		} else if (!jobtitle.equals(other.jobtitle))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (primaryaddress == null) {
			if (other.primaryaddress != null)
				return false;
		} else if (!primaryaddress.equals(other.primaryaddress))
			return false;
		if (secondaryaddress == null) {
			if (other.secondaryaddress != null)
				return false;
		} else if (!secondaryaddress.equals(other.secondaryaddress))
			return false;
		if (telephone == null) {
			if (other.telephone != null)
				return false;
		} else if (!telephone.equals(other.telephone))
			return false;
		if (ticketRequests == null) {
			if (other.ticketRequests != null)
				return false;
		} else if (!ticketRequests.equals(other.ticketRequests))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Employee [emp_id=" + emp_id + ", firstname=" + firstname + ", lastname=" + lastname + ", businessunit="
				+ businessunit + ", jobtitle=" + jobtitle + ", email=" + email + ", telephone=" + telephone
				+ ", primaryaddress=" + primaryaddress + ", secondaryaddress=" + secondaryaddress + ", username="
				+ username + ", password=" + password + ", ticketRequests=" + ticketRequests + "]";
	}
  
}