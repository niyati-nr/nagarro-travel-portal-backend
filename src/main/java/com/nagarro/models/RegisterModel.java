package com.nagarro.models;

public class RegisterModel {
	  private String firstname;
	  private String lastname;
	  private String businessunit;
	  private String jobtitle;
	  private String email;
	  private String telephone;
	  private String addressline1;
	  private String city1;
	  private String state1;
	  private String zipcode1;
	  private String country1;
	  private String addresstype1;
	  private String addressline2;
	  private String city2;
	  private String state2;
	  private String zipcode2;
	  private String country2;
	  private String addresstype2;
	  public RegisterModel() {
		  
	  }
	  
	public RegisterModel(String firstname, String lastname, String businessunit, String jobtitle, String email,
			String telephone, String addressLine1, String city1, String state1, String zipcode1, String country1,
			String addressType1, String addressLine2, String city2, String state2, String zipcode2, String country2,
			String addressType2) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.businessunit = businessunit;
		this.jobtitle = jobtitle;
		this.email = email;
		this.telephone = telephone;
		this.addressline1 = addressLine1;
		this.city1 = city1;
		this.state1 = state1;
		this.zipcode1 = zipcode1;
		this.country1 = country1;
		this.addresstype1 = addressType1;
		this.addressline2 = addressLine2;
		this.city2 = city2;
		this.state2 = state2;
		this.zipcode2 = zipcode2;
		this.country2 = country2;
		this.addresstype2 = addressType2;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getBusinessunit() {
		return businessunit;
	}
	public void setBusinessunit(String businessunit) {
		this.businessunit = businessunit;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getAddressline1() {
		return addressline1;
	}
	public void setAddressline1(String addressLine1) {
		this.addressline1 = addressLine1;
	}
	public String getCity1() {
		return city1;
	}
	public void setCity1(String city1) {
		this.city1 = city1;
	}
	public String getState1() {
		return state1;
	}
	public void setState1(String state1) {
		this.state1 = state1;
	}
	public String getZipcode1() {
		return zipcode1;
	}
	public void setZipcode1(String zipcode1) {
		this.zipcode1 = zipcode1;
	}
	public String getCountry1() {
		return country1;
	}
	public void setCountry1(String country1) {
		this.country1 = country1;
	}
	public String getAddresstype1() {
		return addresstype1;
	}
	public void setAddresstype1(String addressType1) {
		this.addresstype1 = addressType1;
	}
	public String getAddressline2() {
		return addressline2;
	}
	public void setAddressline2(String addressLine2) {
		this.addressline2 = addressLine2;
	}
	public String getCity2() {
		return city2;
	}
	public void setCity2(String city2) {
		this.city2 = city2;
	}
	public String getState2() {
		return state2;
	}
	public void setState2(String state2) {
		this.state2 = state2;
	}
	public String getZipcode2() {
		return zipcode2;
	}
	public void setZipcode2(String zipcode2) {
		this.zipcode2 = zipcode2;
	}
	public String getCountry2() {
		return country2;
	}
	public void setCountry2(String country2) {
		this.country2 = country2;
	}
	public String getAddresstype2() {
		return addresstype2;
	}
	public void setAddresstype2(String addressType2) {
		this.addresstype2 = addressType2;
	}
	  
}
