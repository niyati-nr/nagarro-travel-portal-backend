package com.nagarro.models;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@Entity
@Table(name="documents")
public class Document {
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) Long document_id;
	@NotNull(message = "Document Name is required")
	  @Column(length=250, name="document_name")
	  private String documentName;
	@NotNull(message = "Document Description is required")
	  @Column(length=250, name="description")
	  private String description;
	@NotNull(message = "Document Size is required")
	  @Column(name="size")
	  private Long size;
	@NotNull(message = "File is required")
	@Column(name="file")
	@Lob
	  private byte[] file;
	@NotNull(message = "Ticket Request is required")
	  @ManyToOne
	  @JoinColumn(name="request_id", nullable=false)
	  private TicketRequest ref_request;
	public Document() {
		
	}
	
	public Document(Long document_id,String documentName,String description,Long size,byte[] file,TicketRequest ref_request) {
		this.document_id = document_id;
		this.documentName = documentName;
		this.description = description;
		this.size = size;
		this.file = file;
		this.ref_request = ref_request;
	}

	public Long getDocument_id() {
		return document_id;
	}
	public void setDocument_id(Long document_id) {
		this.document_id = document_id;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public byte[] getFile() {
		return file;
	}
	public void setFile(byte[] file) {
		this.file = file;
	}
	public TicketRequest getRef_request() {
		return ref_request;
	}
	public void setRef_request(TicketRequest ref_request) {
		this.ref_request = ref_request;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((documentName == null) ? 0 : documentName.hashCode());
		result = prime * result + ((document_id == null) ? 0 : document_id.hashCode());
		result = prime * result + Arrays.hashCode(file);
		result = prime * result + ((ref_request == null) ? 0 : ref_request.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (documentName == null) {
			if (other.documentName != null)
				return false;
		} else if (!documentName.equals(other.documentName))
			return false;
		if (document_id == null) {
			if (other.document_id != null)
				return false;
		} else if (!document_id.equals(other.document_id))
			return false;
		if (!Arrays.equals(file, other.file))
			return false;
		if (ref_request == null) {
			if (other.ref_request != null)
				return false;
		} else if (!ref_request.equals(other.ref_request))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Document [document_id=" + document_id + ", documentName=" + documentName + ", description="
				+ description + ", size=" + size + ", file=" + Arrays.toString(file) + ", ref_request=" + ref_request
				+ "]";
	}
	
}
