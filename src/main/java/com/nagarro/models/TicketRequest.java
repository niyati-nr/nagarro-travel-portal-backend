package com.nagarro.models;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nagarro.common.RequestExpenseHandler;
import com.nagarro.common.RequestPriority;
import com.nagarro.common.RequestStatus;
import com.nagarro.common.RequestType;
@Entity
@Table(name="ticketrequests")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
allowGetters = true)
public class TicketRequest {
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) Long request_id;
	  @NotNull(message = "Request Type is required")
	  @Column(name="request_type")
	  @Enumerated(EnumType.STRING)
	  private RequestType requestType;
	  @NotNull(message = "Request Priority is required")
	  @Column(name="request_priority")
	  @Enumerated(EnumType.STRING)
	  private RequestPriority requestPriority;
	  @NotNull(message = "Request Status is required")
	  @Column(name="request_status")
	  @Enumerated(EnumType.STRING)
	  private RequestStatus requestStatus;
//	  @NotNull(message = "Origin is required")
//	  @OneToOne
//	  @JoinColumn(name="fk_origin")
//	  private Airport origin;
//	  @NotNull(message = "Destination is required")
//	  @OneToOne
//	  @JoinColumn(name="fk_destination")
//	  private Airport destination;
	  @NotNull(message = "Origin City is required")
	  @OneToOne
	  @JoinColumn(name="origin_city")
	  private City origin_city;
	  @NotNull(message = "Destination City is required")
	  @OneToOne
	  @JoinColumn(name="destination_city")
	  private City destination_city;
	  @NotNull(message = "Travel Start Date is required")
	  @Column(name="start_date")
	  @Temporal(TemporalType.DATE)
	  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	  private Date start_date;
	  @NotNull(message = "Travel End Date is required")
	  @Temporal(TemporalType.DATE)
	  @Column(name="end_date")
	  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	  private Date end_date;
	  @NotNull(message = "Passport Number is required")
	  @Column(name="passport_number",length=25)
	  private String passport_number;
	  @NotNull(message = "Project Name is required")
	  @Column(name="project_name",length=100)
	  private String project_name;
	  @NotNull(message = "Name of Expense Handler is required")
	  @Column(name="expense_handler")
	  @Enumerated(EnumType.STRING)
	  private RequestExpenseHandler expense_handler;
	  @Column(name="travel_approver",length=100,nullable=true)
	  private String travel_approver;
	  @Column(name="travel_duration",length=100,nullable=true)
	  private String travel_duration;
	  @Column(name="amount_limit",length=500,nullable=true)
	  private String amount_limit;
	  @NotNull(message = "Additional Details are required")
	  @Column(name="additional_details",length=1000)
	  private String additional_details;
	  @NotNull(message = "Employee is required")
	  @ManyToOne
	  @JoinColumn(name="emp_id", nullable=false)
	  private Employee ref_employee;
	  @OneToMany(mappedBy="ref_request", cascade = CascadeType.REMOVE)
	  @JsonIgnore
	  private Set<Document> documents;
//	  @Column(nullable = false, name="last_updated")
//	  @Temporal(TemporalType.TIMESTAMP)
//	    private java.util.Date lastUpdated;
	  @Column(nullable = false, name="last_updated")
	    private String lastUpdated;
	  @Column(name="admin_remarks",length=1000,nullable=true)
	  private String admin_remarks;
	  public TicketRequest() {
		  
	  }
	  
	public TicketRequest(Long request_id, RequestType requestType,
			RequestPriority requestPriority,Date start_date,Date end_date,String passport_number,String project_name,RequestExpenseHandler expense_handler, String travel_approver,
			String travel_duration, String amount_limit,String additional_details,Employee ref_employee, String lastUpdated, Set<Document> documents, City originCity, City destinationCity) {
		this.request_id = request_id;
		this.requestType = requestType;
		this.requestPriority = requestPriority;
//		this.origin = origin;
//		this.destination = destination;
		this.start_date = start_date;
		this.end_date = end_date;
		this.passport_number = passport_number;
		this.project_name = project_name;
		this.expense_handler = expense_handler;
		this.travel_approver = travel_approver;
		this.travel_duration = travel_duration;
		this.amount_limit = amount_limit;
		this.additional_details = additional_details;
		this.ref_employee = ref_employee;
		this.lastUpdated = lastUpdated;
		this.documents = documents;
		this.origin_city = originCity;
		this.destination_city = destinationCity;
	}
	
	public City getOrigin_city() {
		return origin_city;
	}

	public void setOrigin_city(City origin_city) {
		this.origin_city = origin_city;
	}

	public City getDestination_city() {
		return destination_city;
	}

	public void setDestination_city(City destination_city) {
		this.destination_city = destination_city;
	}

	public Long getRequest_id() {
		return request_id;
	}
	public void setRequest_id(Long request_id) {
		this.request_id = request_id;
	}
	public RequestType getRequestType() {
		return requestType;
	}
	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}
	public RequestPriority getRequestPriority() {
		return requestPriority;
	}
	public void setRequestPriority(RequestPriority requestPriority) {
		this.requestPriority = requestPriority;
	}
//	public Airport getOrigin() {
//		return origin;
//	}
//	public void setOrigin(Airport origin) {
//		this.origin = origin;
//	}
//	public Airport getDestination() {
//		return destination;
//	}
//	public void setDestination(Airport destination) {
//		this.destination = destination;
//	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public String getPassport_number() {
		return passport_number;
	}
	public void setPassport_number(String passport_number) {
		this.passport_number = passport_number;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public RequestExpenseHandler getExpense_handler() {
		return expense_handler;
	}
	public void setExpense_handler(RequestExpenseHandler expense_handler) {
		this.expense_handler = expense_handler;
	}
	public String getTravel_approver() {
		return travel_approver;
	}
	public void setTravel_approver(String travel_approver) {
		this.travel_approver = travel_approver;
	}
	public String getTravel_duration() {
		return travel_duration;
	}
	public void setTravel_duration(String travel_duration) {
		this.travel_duration = travel_duration;
	}
	public String getAmount_limit() {
		return amount_limit;
	}
	public void setAmount_limit(String amount_limit) {
		this.amount_limit = amount_limit;
	}
	public String getAdditional_details() {
		return additional_details;
	}
	public void setAdditional_details(String additional_details) {
		this.additional_details = additional_details;
	}
	public Employee getRef_employee() {
		return ref_employee;
	}
	public void setRef_employee(Employee ref_employee) {
		this.ref_employee = ref_employee;
	}
	
	public Set<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}
	

//	public Date getLastUpdated() {
//		return lastUpdated;
//	}
//	public void setLastUpdated(Date lastUpdated) {
//		this.lastUpdated = lastUpdated;
//	}
	
	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getAdmin_remarks() {
		return admin_remarks;
	}

	public void setAdmin_remarks(String admin_remarks) {
		this.admin_remarks = admin_remarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additional_details == null) ? 0 : additional_details.hashCode());
		result = prime * result + ((admin_remarks == null) ? 0 : admin_remarks.hashCode());
		result = prime * result + ((amount_limit == null) ? 0 : amount_limit.hashCode());
		result = prime * result + ((destination_city == null) ? 0 : destination_city.hashCode());
		result = prime * result + ((documents == null) ? 0 : documents.hashCode());
		result = prime * result + ((end_date == null) ? 0 : end_date.hashCode());
		result = prime * result + ((expense_handler == null) ? 0 : expense_handler.hashCode());
		result = prime * result + ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		result = prime * result + ((origin_city == null) ? 0 : origin_city.hashCode());
		result = prime * result + ((passport_number == null) ? 0 : passport_number.hashCode());
		result = prime * result + ((project_name == null) ? 0 : project_name.hashCode());
		result = prime * result + ((ref_employee == null) ? 0 : ref_employee.hashCode());
		result = prime * result + ((requestPriority == null) ? 0 : requestPriority.hashCode());
		result = prime * result + ((requestStatus == null) ? 0 : requestStatus.hashCode());
		result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
		result = prime * result + ((request_id == null) ? 0 : request_id.hashCode());
		result = prime * result + ((start_date == null) ? 0 : start_date.hashCode());
		result = prime * result + ((travel_approver == null) ? 0 : travel_approver.hashCode());
		result = prime * result + ((travel_duration == null) ? 0 : travel_duration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketRequest other = (TicketRequest) obj;
		if (additional_details == null) {
			if (other.additional_details != null)
				return false;
		} else if (!additional_details.equals(other.additional_details))
			return false;
		if (admin_remarks == null) {
			if (other.admin_remarks != null)
				return false;
		} else if (!admin_remarks.equals(other.admin_remarks))
			return false;
		if (amount_limit == null) {
			if (other.amount_limit != null)
				return false;
		} else if (!amount_limit.equals(other.amount_limit))
			return false;
		if (destination_city == null) {
			if (other.destination_city != null)
				return false;
		} else if (!destination_city.equals(other.destination_city))
			return false;
		if (documents == null) {
			if (other.documents != null)
				return false;
		} else if (!documents.equals(other.documents))
			return false;
		if (end_date == null) {
			if (other.end_date != null)
				return false;
		} else if (!end_date.equals(other.end_date))
			return false;
		if (expense_handler != other.expense_handler)
			return false;
		if (lastUpdated == null) {
			if (other.lastUpdated != null)
				return false;
		} else if (!lastUpdated.equals(other.lastUpdated))
			return false;
		if (origin_city == null) {
			if (other.origin_city != null)
				return false;
		} else if (!origin_city.equals(other.origin_city))
			return false;
		if (passport_number == null) {
			if (other.passport_number != null)
				return false;
		} else if (!passport_number.equals(other.passport_number))
			return false;
		if (project_name == null) {
			if (other.project_name != null)
				return false;
		} else if (!project_name.equals(other.project_name))
			return false;
		if (ref_employee == null) {
			if (other.ref_employee != null)
				return false;
		} else if (!ref_employee.equals(other.ref_employee))
			return false;
		if (requestPriority != other.requestPriority)
			return false;
		if (requestStatus != other.requestStatus)
			return false;
		if (requestType != other.requestType)
			return false;
		if (request_id == null) {
			if (other.request_id != null)
				return false;
		} else if (!request_id.equals(other.request_id))
			return false;
		if (start_date == null) {
			if (other.start_date != null)
				return false;
		} else if (!start_date.equals(other.start_date))
			return false;
		if (travel_approver == null) {
			if (other.travel_approver != null)
				return false;
		} else if (!travel_approver.equals(other.travel_approver))
			return false;
		if (travel_duration == null) {
			if (other.travel_duration != null)
				return false;
		} else if (!travel_duration.equals(other.travel_duration))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TicketRequest [request_id=" + request_id + ", requestType=" + requestType + ", requestPriority="
				+ requestPriority + ", requestStatus=" + requestStatus + ", origin_city=" + origin_city
				+ ", destination_city=" + destination_city + ", start_date=" + start_date + ", end_date=" + end_date
				+ ", passport_number=" + passport_number + ", project_name=" + project_name + ", expense_handler="
				+ expense_handler + ", travel_approver=" + travel_approver + ", travel_duration=" + travel_duration
				+ ", amount_limit=" + amount_limit + ", additional_details=" + additional_details + ", ref_employee="
				+ ref_employee + ", documents=" + documents + ", lastUpdated=" + lastUpdated + ", admin_remarks="
				+ admin_remarks + "]";
	}

}