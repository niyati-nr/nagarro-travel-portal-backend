package com.nagarro.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="regions")
public class Region {
	@Column(name="id",columnDefinition = "INT(11) UNSIGNED ")
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) int id;
	@Column(name="name",length=255)
	private String name;
	@Column(name="code",length=10)
	private String code;
	  @ManyToOne
	  @JoinColumn(name="country_id", nullable=false)
	  private Country ref_country;
	  @OneToMany(mappedBy="ref_region",cascade=CascadeType.ALL)
	  @JsonIgnore
	  private Set<City> cities;
	public Region() {
		
	}
	public Region(short id, String name, String code, Country country) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.ref_country = country;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public Country getRef_country() {
		return ref_country;
	}
	public void setRef_country(Country ref_country) {
		this.ref_country = ref_country;
	}
	
	public Set<City> getCities() {
		return cities;
	}
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ref_country == null) ? 0 : ref_country.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ref_country == null) {
			if (other.ref_country != null)
				return false;
		} else if (!ref_country.equals(other.ref_country))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Region [id=" + id + ", name=" + name + ", code=" + code + ", ref_country=" + ref_country + "]";
	}
	
	
}
