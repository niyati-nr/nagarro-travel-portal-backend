package com.nagarro.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cities")
public class City {
	@Column(name="id",columnDefinition = "INT(11) UNSIGNED ")
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) int id;
	@Column(name="name",length=255)
	private String name;
	  @ManyToOne
	  @JoinColumn(name="region_id", nullable=false)
	  private Region ref_region;
	public City() {
		
	}
	
	public City(int id, String name, Region ref_region) {
		this.id = id;
		this.name = name;
		this.ref_region = ref_region;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Region getRef_region() {
		return ref_region;
	}
	public void setRef_region(Region ref_region) {
		this.ref_region = ref_region;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ref_region == null) ? 0 : ref_region.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ref_region == null) {
			if (other.ref_region != null)
				return false;
		} else if (!ref_region.equals(other.ref_region))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", ref_region=" + ref_region + "]";
	}
	
	
}
