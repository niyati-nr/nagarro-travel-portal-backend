package com.nagarro.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TicketRequestFormModel {
	@JsonProperty("request_type")
	  private String requestType;
	@JsonProperty("request_priority")
	  private String requestPriority;
//	  private Long origin_id;
//	  private Long destination_id;
	  private int origin_city;
	  private int destination_city;
	  private String start_date;
	  private String end_date;
	  private String passport_number;
	  private String project_name;
	  @JsonProperty("expense_handler")
	  private String expense_handler;
	  private String travel_approver;
	  private String travel_duration;
	  private String amount_limit;
	  private String additional_details;
	  @JsonProperty("admin_remarks")
	  private String admin_remarks;
	  public TicketRequestFormModel() {
		  
	  }
	  
	

	public TicketRequestFormModel(String requestType, String requestPriority, Long origin_id, Long destination_id,
			String start_date, String end_date, String passport_number, String project_name, String expense_handler,
			String travel_approver, String travel_duration, String amount_limit, String additional_details,
			String admin_remarks, int origin_city, int destination_city) {
		this.requestType = requestType;
		this.requestPriority = requestPriority;
		this.start_date = start_date;
		this.end_date = end_date;
		this.passport_number = passport_number;
		this.project_name = project_name;
		this.expense_handler = expense_handler;
		this.travel_approver = travel_approver;
		this.travel_duration = travel_duration;
		this.amount_limit = amount_limit;
		this.additional_details = additional_details;
		this.admin_remarks = admin_remarks;
		this.origin_city = origin_city;
		this.destination_city = destination_city;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestPriority() {
		return requestPriority;
	}
	public void setRequestPriority(String requestPriority) {
		this.requestPriority = requestPriority;
	}
//	public Long getOrigin_id() {
//		return origin_id;
//	}
//	public void setOrigin_id(Long origin_id) {
//		this.origin_id = origin_id;
//	}
//	public Long getDestination_id() {
//		return destination_id;
//	}
//	public void setDestination_id(Long destination_id) {
//		this.destination_id = destination_id;
//	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getPassport_number() {
		return passport_number;
	}
	public void setPassport_number(String passport_number) {
		this.passport_number = passport_number;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getExpense_handler() {
		return expense_handler;
	}
	public void setExpense_handler(String expense_handler) {
		this.expense_handler = expense_handler;
	}
	public String getTravel_approver() {
		return travel_approver;
	}
	public void setTravel_approver(String travel_approver) {
		this.travel_approver = travel_approver;
	}
	public String getTravel_duration() {
		return travel_duration;
	}
	public void setTravel_duration(String travel_duration) {
		this.travel_duration = travel_duration;
	}
	public String getAmount_limit() {
		return amount_limit;
	}
	public void setAmount_limit(String amount_limit) {
		this.amount_limit = amount_limit;
	}
	public String getAdditional_details() {
		return additional_details;
	}
	public void setAdditional_details(String additional_details) {
		this.additional_details = additional_details;
	}

	public String getAdmin_remarks() {
		return admin_remarks;
	}

	public void setAdmin_remarks(String admin_remarks) {
		this.admin_remarks = admin_remarks;
	}
	public int getOrigin_city() {
		return origin_city;
	}
	public void setOrigin_city(int origin_city) {
		this.origin_city = origin_city;
	}
	public int getDestination_city() {
		return destination_city;
	}
	public void setDestination_city(int destination_city) {
		this.destination_city = destination_city;
	}
	
}
