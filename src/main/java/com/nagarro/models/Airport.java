package com.nagarro.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="airports")
public class Airport {
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) Long airport_id;
	  @Column(name="country_code")
	  private String country_code;
	  @Column(name="region_name")
	  private String region_name;
	  @Column(name="iata_code",length=3)
	  private String iata_code;
	  @Column(name="airport_name")
	  private String airport_name;
	  public Airport() {
		  
	  }
	public Airport(String country_code, String region_name, String iata_code, String airport_name) {
		this.country_code = country_code;
		this.region_name = region_name;
		this.iata_code = iata_code;
		this.airport_name = airport_name;
	}
	
	public Long getAirport_id() {
		return airport_id;
	}
	public void setAirport_id(Long airport_id) {
		this.airport_id = airport_id;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getIata_code() {
		return iata_code;
	}
	public void setIata_code(String iata_code) {
		this.iata_code = iata_code;
	}
	public String getAirport_name() {
		return airport_name;
	}
	public void setAirport_name(String airport_name) {
		this.airport_name = airport_name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airport_name == null) ? 0 : airport_name.hashCode());
		result = prime * result + ((country_code == null) ? 0 : country_code.hashCode());
		result = prime * result + ((iata_code == null) ? 0 : iata_code.hashCode());
		result = prime * result + ((region_name == null) ? 0 : region_name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airport other = (Airport) obj;
		if (airport_name == null) {
			if (other.airport_name != null)
				return false;
		} else if (!airport_name.equals(other.airport_name))
			return false;
		if (country_code == null) {
			if (other.country_code != null)
				return false;
		} else if (!country_code.equals(other.country_code))
			return false;
		if (iata_code == null) {
			if (other.iata_code != null)
				return false;
		} else if (!iata_code.equals(other.iata_code))
			return false;
		if (region_name == null) {
			if (other.region_name != null)
				return false;
		} else if (!region_name.equals(other.region_name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Airport [country_code=" + country_code + ", region_name=" + region_name + ", iata_code=" + iata_code
				+ ", airport_name=" + airport_name + "]";
	}
	  
}
