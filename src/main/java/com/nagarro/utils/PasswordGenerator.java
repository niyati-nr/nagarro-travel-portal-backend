package com.nagarro.utils;

import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class PasswordGenerator {
	private static final String SECRET_KEY = "7706797289E8D29D5995FE740F9AA07B";
	private static final String SALT = "B7D875865921E751";
    public static String getAlphaNumericString()
    {
    	//length of password
    	int n=8;
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n); 
        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
    public static String encodePassword(String str) {
    	try {
    	      byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    	      IvParameterSpec ivspec = new IvParameterSpec(iv);
    	 
    	      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    	      KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 65536, 256);
    	      SecretKey tmp = factory.generateSecret(spec);
    	      SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
    	 
    	      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    	      cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
    	      return Base64.getEncoder()
    	          .encodeToString(cipher.doFinal(str.getBytes(StandardCharsets.UTF_8)));
    	    } catch (Exception e) {
    	      System.out.println("Error while encrypting: " + e.toString());
    	    }
    	    return null;
    }
    public static String decodePassword(String str) {
    	try {
    	      byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    	      IvParameterSpec ivspec = new IvParameterSpec(iv);
    	 
    	      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    	      KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 65536, 256);
    	      SecretKey tmp = factory.generateSecret(spec);
    	      SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
    	 
    	      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
    	      cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
    	      return new String(cipher.doFinal(Base64.getDecoder().decode(str)));
    	    } catch (Exception e) {
    	      System.out.println("Error while decrypting: " + e.toString());
    	    }
    	    return null;
    }
}
