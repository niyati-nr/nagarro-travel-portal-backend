package com.nagarro.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
public class DateParser {
	public static java.util.Date parseStringToUtilDate(String str) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
		format.setTimeZone(TimeZone.getDefault());
		java.util.Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	public static java.sql.Date parseStringToSqlDate(String str){
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
		format.setTimeZone(TimeZone.getDefault());
		java.util.Date utildate = parseStringToUtilDate(str);
		java.sql.Date sqldate = new java.sql.Date(utildate.getTime());
		return sqldate;
	}
}
