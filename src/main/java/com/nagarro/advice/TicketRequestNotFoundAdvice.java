package com.nagarro.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.nagarro.exception.TicketRequestNotFoundException;

@ControllerAdvice
public class TicketRequestNotFoundAdvice {
	@ResponseBody
	  @ExceptionHandler(TicketRequestNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String ticketRequestNotFoundHandler(TicketRequestNotFoundException ex) {
	    return ex.getMessage();
	  }
}
