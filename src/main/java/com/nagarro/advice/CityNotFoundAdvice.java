package com.nagarro.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.nagarro.exception.CityNotFoundException;

@ControllerAdvice
public class CityNotFoundAdvice {
	@ResponseBody
	  @ExceptionHandler(CityNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String countryNotFoundHandler(CityNotFoundException ex) {
	    return ex.getMessage();
	  }
}
