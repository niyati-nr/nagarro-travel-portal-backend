package com.nagarro.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.nagarro.exception.DocumentNotFoundException;

@ControllerAdvice
public class DocumentNotFoundAdvice {
	@ResponseBody
	  @ExceptionHandler(DocumentNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String documentNotFoundHandler(DocumentNotFoundException ex) {
	    return ex.getMessage();
	  }
}
