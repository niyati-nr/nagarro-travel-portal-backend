package com.nagarro.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.nagarro.exception.RegionNotFoundException;

@ControllerAdvice
public class RegionNotFoundAdvice {
	@ResponseBody
	  @ExceptionHandler(RegionNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String countryNotFoundHandler(RegionNotFoundException ex) {
	    return ex.getMessage();
	  }
}
